import org.junit.jupiter.api.Test;
import resources.CalculatorResource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300+100";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 - 1";
        assertEquals(200, calculatorResource.calculate(expression));

        expression = "100*20*10";
        assertEquals(20000, calculatorResource.calculate(expression));

        expression = " 300 / 10";
        assertEquals(30, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSum2(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals(401, calculatorResource.sum2(expression));

        expression = "300+99+3";
        assertEquals(402, calculatorResource.sum2(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testSubtraction2(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-1";
        assertEquals(898, calculatorResource.subtraction2(expression));

        expression = "20-2-1";
        assertEquals(17, calculatorResource.subtraction2(expression));
    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "900*100";
        assertEquals(90000, calculatorResource.multiplication(expression));

        expression = "20*2";
        assertEquals(40, calculatorResource.multiplication(expression));
    }

    @Test
    public void testMultiplication2() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "900*100*10";
        assertEquals(900000, calculatorResource.multiplication2(expression));

        expression = "20*2*2";
        assertEquals(80, calculatorResource.multiplication2(expression));
    }

    @Test
    public void testDivision() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "900/10";
        assertEquals(90, calculatorResource.division(expression));

        expression = "20/2";
        assertEquals(10, calculatorResource.division(expression));
    }

    @Test
    public void testDivision2() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "900/10/10";
        assertEquals(9, calculatorResource.division2(expression));

        expression = "20/2/2";
        assertEquals(5, calculatorResource.division2(expression));
    }
}
